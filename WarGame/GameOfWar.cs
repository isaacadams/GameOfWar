﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarGame
{
    public enum ECards { Two = 2, Three = 3, Four = 4, Five = 5, Six = 6, Seven = 7, Eight = 8, Nine = 9, Ten = 10, Jack = 11, Queen = 12, King = 13, Ace = 14 }
    //public enum EPlayers { User, Computer }

    class GameOfWar
    {    
        Deck GameDeck;
        //Dictionary<EPlayers, Player> Players;
        List<Player> Players;
        const string ComputerPlayerName = "Computer";
        const string UserPlayerName = "User";

        Player Computer { get { return Players.Find(plyer => plyer.Name == ComputerPlayerName); } set { } }
        Player User { get { return Players.Find(plyer => plyer.Name == UserPlayerName); } set { } }

        public bool GameOver;

        public void Setup()
        {
            GameDeck = new Deck();
            Players = new List<Player>();
            GameOver = false;

            Players.Add(new Player(ComputerPlayerName));
            Players.Add(new Player(UserPlayerName));

            GameDeck.Shuffle();
            GameDeck.Deal(Players);

            string message = "Welcome to a new game of war! It is you versus the computer.\n" +
                             "To play, simply press the enter key and that will trigger the first draw of the game.\n" +
                             "Simply press the enter key everytime you want to draw.\n" +
                             string.Format("Computer's Hand: {0}\n Your Hand: {1}", Computer.MyHand.Count, User.MyHand.Count);

            Console.WriteLine(message);
        }

        public void Draw(int NumberOfCards = 1)
        {
            //Both Players Draw 1 or 3 cards, default is 1
            Computer.Draw(NumberOfCards);
            User.Draw(NumberOfCards);

            //Show the game player which cards were just put into play
            Console.WriteLine();
            Console.WriteLine("Computer's Card: " + ((ECards)Computer.CardInPlay).ToString());
            Console.WriteLine("Your Card: " + ((ECards)User.CardInPlay).ToString());

            //Compare the cards in play to determine whether it is a tie
            GameLogic();
        }

        void RoundResultsMessage(string message)
        {
            if (GameOver)
            {
                //Console.Beep();
                Console.WriteLine("GAME OVER!");
                Console.WriteLine("Game Result: " + message);
            }
            else
                Console.WriteLine("Round Result: " + message);

            Console.WriteLine("Computer's Hand: " + Computer.MyHand.Count);
            Console.WriteLine("Your Hand: " + User.MyHand.Count);
        }

        string CompareCardsInPlay()
        {
            string message = "";
            bool Tie = Computer.CardInPlay == User.CardInPlay;

            if(!Tie)
            {
                if (Computer.CardInPlay > User.CardInPlay)
                {
                    Computer.Win(User.MyStack);
                    message = "The Computer beat you.";
                }
                else
                {
                    User.Win(Computer.MyStack);
                    message = "You beat the computer!";
                }
            }

            return message;
        }

        void GameLogic()
        {
            string message = CompareCardsInPlay();
            bool Tie = string.IsNullOrEmpty(message);

            //Either the computer's card is greater, the players card is greater, or they are equal
            if (!Tie)
            {
                GameOver = Computer.LastCard || User.LastCard;
                RoundResultsMessage(message);
            }
            else
            {
                foreach (var gamePlayer in Players)
                {
                    //If the play is down to their last card, has more cards in their stack, and it is a tie give them the chance to win
                    if (gamePlayer.LastCard && gamePlayer.MyStack.Count > 1)
                    {
                        message = LastCardTieBreaker(gamePlayer, GetOtherPlayer(gamePlayer.Name));
                        GameOver = true;
                        RoundResultsMessage(message);
                        break;
                    }

                    //If the player is down to their last card, have no cards left in their stack, and it is a tie, then it is gameover for them
                    if (gamePlayer.LastCard && gamePlayer.MyStack.Count <= 1)
                    {
                        message = string.Format("The {0} lost the game", gamePlayer.Name);
                        GameOver = true;
                        RoundResultsMessage(message);
                        break;
                    }
                }

                if (!GameOver)
                {
                    //If Neither player is at their last card, then it is a normal war scenario
                    Console.WriteLine("War! Press a key to move forward.");
                    //Console.ReadKey();
                    Draw(4);
                }
            }
        }

        string LastCardTieBreaker(Player PlayerWithLastCard, Player OtherPlayer)
        {
            string message = "";
            bool Tie = true;
            
            while (Tie)
            {
                //If the player with last card has no cards left in his stack, then they have lost the game
                if (PlayerWithLastCard.MyStack.Count == 0)
                {
                    message = string.Format("The {0} lost the game", PlayerWithLastCard.Name);
                    Tie = false;
                }
                else
                {
                    //The CardInPlay was the last card in the stack and has already been compared,
                    //we want to compare the card previous to the card in play, which is the next card in the stack
                    PlayerWithLastCard.MyStack.Pop();
                    //Keep checking to see if there is a tie until the game is resolved.
                    //The game will be able to end since the other player's card is kept constant and there can only be three other
                    //possible occurences of that card in the game.
                    message = CompareCardsInPlay();
                    Tie = string.IsNullOrEmpty(message);
                }
            }

            return message;
        }

        Player GetOtherPlayer(string name)
        {
            Player otherPlayer = name == ComputerPlayerName ? Computer : User;
            return otherPlayer;
        }
    }
}
