﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarGame
{
    class Program
    {
        public static GameOfWar myGameOfWar = new GameOfWar();        
        static void Main(string[] args)
        {
            while (true)
            {
                myGameOfWar.Setup();
                while (!myGameOfWar.GameOver)
                {
                    //Console.ReadKey();
                    //Player hits key and draw method is called
                    myGameOfWar.Draw();
                }
            }
            //Console.ReadKey();
        }
    }
}
