﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarGame
{
    public interface ICardGamePlayer
    {
        string Name { get; set; }
        Queue<int> MyHand { get; set; }
        void Draw(int NumberOfCards);
    }

    public class Player : ICardGamePlayer
    {
        Queue<int> _myHand;

        public string Name { get; set; }

        public Queue<int> MyHand { get { return _myHand; } set { } }

        public Stack<int> MyStack;
        public int CardInPlay { get { return MyStack.First(); } set { } }
        public bool LastCard { get { return MyHand.Count == 0; } set { } }

        public Player(string name)
        {
            Name = name;
            _myHand = new Queue<int>();
            MyStack = new Stack<int>();
        }

        public void Draw(int NumberOfCards)
        {
            for (int i = 0; i < NumberOfCards; i++)
            {
                if (_myHand.Count == 0)
                    break;

                MyStack.Push(_myHand.Dequeue());
            }
        }

        public void Win(Stack<int> OtherPlayersStack)
        {
            int count; //Use this for getting Stack Count, 
            //MUST USE THIS VARIABLE otherwise Stack.Count will decrease in every iteration
            //which will mess up the for loop

            //Take your stack and put it into your hand
            count = MyStack.Count;
            for (int i = 0; i < count; i++)
                _myHand.Enqueue(MyStack.Pop());

            count = OtherPlayersStack.Count;
            //Take the other players stack and put it into your hand
            for (int i = 0; i < count; i++)
                _myHand.Enqueue(OtherPlayersStack.Pop());
        }
    }
}
