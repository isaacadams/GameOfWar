﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarGame
{
    public interface IDeck
    {
        void Deal(List<Player> GamePlayers);
        void Shuffle();
    }

    class Deck : IDeck
    {
        int[] Cards;
        Random r;

        public Deck()
        {
            Cards = new int[52];

            var query =
                from suit in new[] { 1, 2, 3, 4 }
                from rank in Enumerable.Range(2, 13)
                select rank;
            Cards = query.ToArray();
        }

        public void Deal(List<Player> GamePlayers)
        {
            int NumberOfCardsPerPlayer = Cards.Length / GamePlayers.Count;
            Stack<int> cardStack = new Stack<int>(Cards);

            foreach (Player player in GamePlayers)
                for (int i = 0; i < NumberOfCardsPerPlayer; i++)
                    player.MyHand.Enqueue(cardStack.Pop());
        }

        public void Shuffle()
        {
            r = new Random();
            for (int n = Cards.Length - 1; n > 0; --n)
            {
                int k = r.Next(n + 1);
                int temp = Cards[n];
                Cards[n] = Cards[k];
                Cards[k] = temp;
            }
        }
    }

    
}
